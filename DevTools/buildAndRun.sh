#!/bin/bash
dockerImageName=element/spring-boot-docker-base
./gradlew bootBuildImage --imageName=$dockerImageName
docker run -p 8080:8080 $dockerImageName
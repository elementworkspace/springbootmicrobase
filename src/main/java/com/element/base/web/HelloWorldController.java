package com.element.base.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.MessageFormat;

@RestController
@RequestMapping("/api/greeting")
public class HelloWorldController {

    @Value("${spring.application.name}")
    private String appName;

    @GetMapping("/")
    public String greeting(@RequestParam(value = "name", defaultValue = "World") String name) {
        return MessageFormat.format( "Hello " + name + "! this is a successful response to the app => {0}.",appName);
    }
}
